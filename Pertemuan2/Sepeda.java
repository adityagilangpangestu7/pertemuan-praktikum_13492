package Pertemuan2;

public class Sepeda {
     //ini adalah tempat atribut
    int gear = 5;

    public Sepeda (int jmlRoda, String jeniSepeda, String merkSepeda){
        System.out.println("Sepeda "+jeniSepeda+" Bermerk "+merkSepeda+" memiliki roda berjumlah "+jmlRoda);
    }
    
     //ini adalah tempat method
    void ngerem() { 
    System.out.println("Sepeda direm"); 
    }

    public static void main(String[] args) {
             //objek baru dan modifikasi
            Sepeda sepedaGunung = new Sepeda(2,"Gunung","United"); 
            Sepeda sepedaHarian = new Sepeda(2, "Harian", "Polygon"); 

            int gearSepeda = sepedaGunung.gear;
            System.out.println("Jumlah gear sepeda ada "+gearSepeda);
            sepedaGunung.ngerem();
            sepedaHarian.ngerem();

    }
    
}
